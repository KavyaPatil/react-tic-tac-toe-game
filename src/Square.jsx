import React, {useState} from 'react';
import "./Square.css";


const Square = ({value, index, squareClick}) => {


  return (
    <div className='square' onClick={() => {
        squareClick(index);
    }}>
    {value}
    </div>
  )
}

export default Square;