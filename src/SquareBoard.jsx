import React, { useState } from "react";
import Square from "./Square";
import "./SquareBoard.css";



const SquareBoard = () => {
  const [value, setValue] = useState(Array(9).fill(null));
  const [player, setPlayer] = useState(true);

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }

  const handleClick = (i) => {
    if (!value.includes(null) || value[i] !== null || calculateWinner(value)) {
      return;
    }

    if (player) {
      const updated = [...value];
      updated[i] = "X";

      setValue(updated);
    } else {
      const updated = [...value];
      updated[i] = "O";

      setValue(updated);
    }

    setPlayer(!player);
  };

let status ='';
  let winner = calculateWinner(value);
  if(winner){
    status = "winner is : " + winner;
  } else {
    status = `Next Player : ${player ? "X" : "O" }`;
  }
  
  return (
    <>
    <div>{status}</div><br />
      <div className="flex">
        <Square value={value[0]} squareClick={handleClick} index="0"></Square>
        <Square value={value[1]} squareClick={handleClick} index="1"></Square>
        <Square value={value[2]} squareClick={handleClick} index="2"></Square>
      </div>
      <div className="flex">
        <Square value={value[3]} squareClick={handleClick} index="3"></Square>
        <Square value={value[4]} squareClick={handleClick} index="4"></Square>
        <Square value={value[5]} squareClick={handleClick} index="5"></Square>
      </div>
      <div className="flex">
        <Square value={value[6]} squareClick={handleClick} index="6"></Square>
        <Square value={value[7]} squareClick={handleClick} index="7"></Square>
        <Square value={value[8]} squareClick={handleClick} index="8"></Square>
      </div>
    </>
  );
};

export default SquareBoard;
